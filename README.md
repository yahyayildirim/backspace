# Gnome Nautilus Backspace
## Gnome Nautilus Dosya Yöneticisinde klavyeden backspace tuşu ile geri gelmeyi aktifleştirmek....
* v1 olan Nautilus 3.x sürümleri için,
* v2 olan Nautilus 4.0 ile 4.6 sürümleri arası için,
* v3 olan ise Nautilus 4.7 sürümü içindir.

### v1 kurulum (Nautilus 3.x)
```
curl -skL https://gitlab.com/yahyayildirim/backspace/-/raw/main/backspace_v1.sh | sudo bash -
```

### v2 kurulum (Nautilus 4.0 ile 4.6)
```
curl -skL https://gitlab.com/yahyayildirim/backspace/-/raw/main/backspace_v2.sh | sudo bash -
```

### v3 kurulum (Nautilus 4.7 ve üzeri)
```
curl -skL https://gitlab.com/yahyayildirim/backspace/-/raw/main/backspace_v3.sh | sudo bash -
```